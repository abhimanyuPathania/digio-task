function hasOnlyDigits(value) {
  return /^\d+$/.test(value)
}

export function validateEmail(email) {
  if (!email) return false;

  // Regex can be stronger. We can even use a 3'rd party service to detect temporary inboxes.
  const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; // eslint-disable-line
  return reg.test(email);
}


export function validateAadhaar(n) {
  if (!n) return false;

  if (n.trim().length !== 12) return false;

  // Don't try to get into string <-> number conversions
  // Keep validations simple
  return hasOnlyDigits(n.trim());
}

export function validateOtp(n) {
  if (!n) return false;

  if (n.trim().length !== 6) return false;

  return hasOnlyDigits(n.trim());
}
