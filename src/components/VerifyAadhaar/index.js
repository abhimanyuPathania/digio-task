import React, { Component } from 'react'
import PropTypes from 'prop-types';
import classnames from 'classnames';

import  { validateAadhaar, validateOtp } from '../../libs/utilities';
import aadhaarLogo from '../../images/aadhaar-logo.png';
import classes from './styles.module.css';

export default class VerifyAadhaar extends Component {
  static propTypes = {
    className: PropTypes.string,
    onVerificationSuccess: PropTypes.func
  }

  constructor(props) {
    super(props);

    this.state = {
      aadhaarInput: '',
      otpInput: '',
      checkboxInput: false,
      isAadhaarValid: false,
      isOtpValid: false,
    }
  }

  onSubmit = (e) => {
    e.preventDefault();

    if (!this.isAllowedToSubmit()) return;

    const { onVerificationSuccess } = this.props;

    if (onVerificationSuccess) onVerificationSuccess();
  }

  isAllowedToSubmit = () => {
    const { isOtpValid, checkboxInput } = this.state;

    return isOtpValid && checkboxInput;
  }

  handleOtpInput = (e) => {
    const userInput = e.target.value;
    this.setState({
      otpInput: e.target.value,
      isOtpValid: validateOtp(userInput)
    });
  }

  handleAadhaarInput = (e) => {
    const userInput = e.target.value;

    this.setState({
      aadhaarInput: e.target.value,
      isAadhaarValid: validateAadhaar(userInput)
    });
  }

  handleCheckboxInput = (e) => {
    this.setState({
      checkboxInput: e.target.checked
    });
  }

  render() {
    const { className } = this.props;
    const {
      aadhaarInput,
      isAadhaarValid,
      otpInput,
      checkboxInput,
    } = this.state;

    const enableSubmit = this.isAllowedToSubmit();

    return (
      <div className={classnames(classes.main, className)}>
        <form onSubmit={this.onSubmit}>
          <div className={classes.aadhaarField}>
            <img src={aadhaarLogo} alt="aadhaar logo" className={classes.aadhaarLogo} />
            <div className={classes.aadhaarFieldContent}>
              <div className={classes.aadhaarFieldInputWrapper}>
                <input
                  type="text"
                  placeholder="Enter your aadhaar"
                  value={aadhaarInput}
                  onChange={this.handleAadhaarInput}
                />
                <button disabled={!isAadhaarValid} type="button">Verify</button>
              </div>

              <div className={classes.consentField}>
                <input
                  type="checkbox"
                  checked={checkboxInput}
                  onChange={this.handleCheckboxInput}
                />
                <span>
                  I agree to eSign this KYC document to get started
                </span>
              </div>
            </div>
          </div>

          <div className={classes.otpField}>
            <input
              type="text"
              placeholder="Enter OTP"
              value={otpInput}
              onChange={this.handleOtpInput}
            />
            <button disabled={!enableSubmit} type="submit">Submit</button>
          </div>
        </form>
      </div>
    )
  }
}
