import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Signin from "./Signin";
import Doc from "./Doc";

import classes from "./styles.module.css";

export default function AppRouter() {
  return (
    <Router>
      <div className={classes.appRouter}>
        <Route path="/" exact component={Signin} />
        <Route path="/doc" component={Doc} />
      </div>
    </Router>
  );
}
