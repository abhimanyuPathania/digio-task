import React from "react";

import VerifyAadhaar from '../../components/VerifyAadhaar';

import classes from './styles.module.css';
export default class Doc extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAadhaarVerified: false,
      showAadharVerifiedBanner: false
    }
  }

  handleVerificationSuccess = () => {
    this.setState({
      isAadhaarVerified: true,
      showAadharVerifiedBanner: true
    }, () => {
      setTimeout(() => {
        this.setState({ showAadharVerifiedBanner: false })
      }, 3000);
    });
  }

  renderNavbar = () => {
    const { isAadhaarVerified } = this.state;

    if (!isAadhaarVerified) {
      return (
        <div className={classes.unverifiedNav}>
          <span>Register Aadhaar</span>
        </div>
      )
    }

    return (
      <div className={classes.navbarSigning}>
        <span>Signing ...</span>
        <a href="https://www.digio.in/#/index">Mutual Non-Disclosure Agreement</a>
      </div>
    );
  }

  renderVerifyAadharModal = () => {
    const { isAadhaarVerified } = this.state;

    if (isAadhaarVerified) return null;

    return (
      <div className={classes.overlay}>
        <div className={classes.verifyAadhaarWrapper}>
          <VerifyAadhaar
            className={classes.verifyAadhaar}
            onVerificationSuccess={this.handleVerificationSuccess}
          />
        </div>
      </div>
    );
  }

  renderVerifiedBanner = () => {
    const { showAadharVerifiedBanner } = this.state;

    if (!showAadharVerifiedBanner) return null;

    return (
      <div className={classes.verifiedBanner}>
        <div className={classes.tick}>✓</div>
        <span>Aadhaar verified successfully</span>
      </div>
    );
  }

  render() {
    return (
      <div className={classes.root}>
        <div className={classes.navbar}>
          {this.renderNavbar()}
        </div>

        <div className={classes.pageContent}>
          <div className={classes.document}>
            <h2>This is the document</h2>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
          </div>

          {this.renderVerifiedBanner()}
          {this.renderVerifyAadharModal()}
        </div>
      </div>
    );
  }
}
