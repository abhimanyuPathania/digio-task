import React  from "react";

import { validateEmail } from '../../libs/utilities';
import genericLogo from '../../images/generic-logo.png';
import googleIcon from '../../images/google-plus.png';
import digioLogo from '../../images/digio_logo.jpg';
import classes from "./styles.module.css";

export default class Signin extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      emailInput: '',
      isValidEmail: false
    }
  }

  handleContinue = () => {
    // history is injected by react-router into props
    const { history } = this.props;
    const { isValidEmail } = this.state;

    if (!isValidEmail) return;

    // replace does not allow user to go back using
    // the browser's back button
    history.replace('/doc');
  }

  handleEmailInput = (e) => {
    const text = e.target.value;

    this.setState({
      emailInput: text,
      isValidEmail: validateEmail(text)
    });
  }

  handleEmailInputEnter = (e) => {
    if (e.key !== 'Enter') return;

    this.handleContinue();
  }

  render() {
    const  { emailInput, isValidEmail } = this.state;

    return (
      <div className={classes.root}>
        <div className={classes.navbar}>
          <div className={classes.navbarText}>
            <span className={classes.navHeading}>Sign document using</span>
            <span className={classes.navSubheading}>sanket@digio.in</span>
          </div>
          <div className={classes.logoWrapper}>
            <img className={classes.logo} src={genericLogo} alt="logo"/>
          </div>
        </div>

        <div className={classes.pageContent}>
          <div className={classes.googleAuth}>
            <div className={classes.emailText}>
              <span>sanket@digio.in uses Gmail?</span>
              <span>login using Google</span>
            </div>

            <button className={classes.googleBtn}>
              <div className={classes.googleBtnContent}>
                <img className={classes.googleIcon} alt="Google+" src={googleIcon}/>
                <span>Google</span>
              </div>
            </button>
          </div>

          <div className={classes.orSection}>
            <div className={classes.bar} />
            <span>OR</span>
          </div>

          <div className={classes.emailAuth}>
            <span className={classes.inputLabel}>Proceed with your email address</span>
            <input
              value={emailInput}
              placeholder="Enter your email"
              onChange={this.handleEmailInput}
              onKeyPress={this.handleEmailInputEnter}
            />
            <div className={classes.terms}>
              By continuing, I confirm to the <b>Terms and Service</b> and <b>Privacy Policy</b> of <a href="https://www.digio.in/#/index">Digio.in</a>
            </div>
            <button
              onClick={this.handleContinue}
              disabled={!isValidEmail}
              className={classes.continueBtn}
            >
              continue
            </button>
        </div>
        </div>
        <div className={classes.footer}>
          <div className={classes.footerLeft}>
            <img src={digioLogo} alt="digilogo" className={classes.footerLogo} />
            <div className={classes.poweredBy}>
              <span>Powered by</span>
              <a href="https://www.digio.in/#/index">www.digio.in</a>
            </div>
          </div>

          <div className={classes.footerRight}>
            <span className={classes.big}>1</span>/3 steps
          </div>
        </div>
      </div>
    );
  }
}
